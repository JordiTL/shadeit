/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef WGLHOLDER_H
#define WGLHOLDER_H

#include <QWidget>
#include <QString>

namespace Ui {
class WGLHolder;
}

class WGLHolder : public QWidget
{
    Q_OBJECT

private:
    void _onCreation();

public:
    explicit WGLHolder(QWidget *parent = 0);
    ~WGLHolder();

private:
    Ui::WGLHolder *ui;

signals:
    void updateShaderLogAction(const QString& log);
    void updateVertShaderViewerAction(const QString& code);
    void updateFragShaderViewerAction(const QString& code);
    void updatePPVertShaderViewerAction(const QString& code);
    void updatePPFragShaderViewerAction(const QString& code);

    void compileShadersAction();

    void updateGLAction();
    void loadMeshAction(const QString& filename);



public slots:
    void noMaterialToggleReaction(bool value);
    void wireframeToggleReaction(bool value);
    void multisamplingToggleReaction(bool value);
    void modelRotationToggleReaction(bool value);
    void lightsRotationToggleReaction(bool value);
    void twoLightsToggleReaction(bool value);

    void updateShaderLogReaction(const QString& log);

    void updateVertShaderViewerReaction(const QString& code);
    void updateFragShaderViewerReaction(const QString& code);
    void updatePPVertShaderViewerReaction(const QString& code);
    void updatePPFragShaderViewerReaction(const QString& code);

    void compileShadersReaction();
    void updateGLReaction();

    // Yes, this is a fucking shit, but be happy =)
    void updateLight0PositionXReaction(double value);
    void updateLight0PositionYReaction(double value);
    void updateLight0PositionZReaction(double value);

    void updateLight0AmbientRReaction(double value);
    void updateLight0AmbientGReaction(double value);
    void updateLight0AmbientBReaction(double value);

    void updateLight0DiffuseRReaction(double value);
    void updateLight0DiffuseGReaction(double value);
    void updateLight0DiffuseBReaction(double value);

    void updateLight0SpecularRReaction(double value);
    void updateLight0SpecularGReaction(double value);
    void updateLight0SpecularBReaction(double value);

    void updateLight1PositionXReaction(double value);
    void updateLight1PositionYReaction(double value);
    void updateLight1PositionZReaction(double value);

    void updateLight1AmbientRReaction(double value);
    void updateLight1AmbientGReaction(double value);
    void updateLight1AmbientBReaction(double value);

    void updateLight1DiffuseRReaction(double value);
    void updateLight1DiffuseGReaction(double value);
    void updateLight1DiffuseBReaction(double value);

    void updateLight1SpecularRReaction(double value);
    void updateLight1SpecularGReaction(double value);
    void updateLight1SpecularBReaction(double value);

    void updateMaterialAmbientRReaction(double value);
    void updateMaterialAmbientGReaction(double value);
    void updateMaterialAmbientBReaction(double value);

    void updateMaterialDiffuseRReaction(double value);
    void updateMaterialDiffuseGReaction(double value);
    void updateMaterialDiffuseBReaction(double value);

    void updateMaterialSpecularRReaction(double value);
    void updateMaterialSpecularGReaction(double value);
    void updateMaterialSpecularBReaction(double value);

    void updateMaterialShininessReaction(double value);

    void loadMeshReaction(const QString& filename);

};

#endif // WGLHOLDER_H
