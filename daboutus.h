/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef DABOUTUS_H
#define DABOUTUS_H

#include <QDialog>

namespace Ui {
class daboutus;
}

class daboutus : public QDialog
{
    Q_OBJECT

public:
    explicit daboutus(QWidget *parent = 0);
    ~daboutus();

private:
    Ui::daboutus *ui;
};

#endif // DABOUTUS_H
