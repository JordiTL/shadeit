/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include <QtGui>

 #include "loghighlighter.h"

 LogHighlighter::LogHighlighter(QTextDocument *parent)
     : QSyntaxHighlighter(parent)
 {
     HighlightingRule rule;

     titleFormat.setFontWeight(QFont::Bold);
     titleFormat.setForeground(Qt::black);
     errorFormat.setBackground(Qt::blue);
     rule.pattern = QRegExp("<[A-Za-z: 0-9]+>");
     rule.format = titleFormat;
     highlightingRules.append(rule);

     errorFormat.setForeground(Qt::black);
     errorFormat.setBackground(Qt::red);
     rule.pattern = QRegExp("ERROR[A-Za-z: 0-9]+",Qt::CaseInsensitive);
     rule.format = errorFormat;
     highlightingRules.append(rule);

     warningFormat.setForeground(Qt::black);
     warningFormat.setBackground(Qt::yellow);
     rule.pattern = QRegExp("WARNING[A-Za-z: 0-9]+",Qt::CaseInsensitive);
     rule.format = warningFormat;
     highlightingRules.append(rule);


 }

 void LogHighlighter::highlightBlock(const QString &text)
 {
     foreach (const HighlightingRule &rule, highlightingRules) {
         QRegExp expression(rule.pattern);
         int index = expression.indexIn(text);
         while (index >= 0) {
             int length = expression.matchedLength();
             setFormat(index, length, rule.format);
             index = expression.indexIn(text, index + length);
         }
     }
     setCurrentBlockState(0);
 }
