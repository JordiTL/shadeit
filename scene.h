/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef SCENE_H
#define SCENE_H
#include <vector>
#include <map>
#include <string>
#include <QGLBuffer>
#include <QPixmap>
#include <assimp/scene.h>

class Scene{ 
private:
    struct MeshData{
        float * _vertexArray;
        float * _normalArray;
        float * _uvArray;
        float * _colorArray;

        unsigned int _vertexCount;
        unsigned int _textureId;
    };

    std::vector<MeshData> _meshes;


    std::map<unsigned int, unsigned int> _textures; //<! (MATERIAL INDEX, GL INDEX)
    std::vector<QPixmap *> _texturesData;

private:
    void _loadTextures(const aiScene* scene, QGLWidget * widgetContex, const QString& pathPrefix = "");

public:
    Scene();
    ~Scene();

    bool loadFromFile(const std::string& filename, QGLWidget * widgetContex);
    void render();

    void reset(QGLWidget * widgetContex);
};

#endif // SCENE_H
