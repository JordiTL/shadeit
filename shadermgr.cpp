/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "shadermgr.h"
#include <QString>

ShaderMgr::ShaderMgr(QObject *parent) :
    QObject(parent)
{
    _fragShader = new QGLShader(QGLShader::Fragment);
    _vertShader = new QGLShader(QGLShader::Vertex);
    _ppfragShader = new QGLShader(QGLShader::Fragment);
    _ppvertShader = new QGLShader(QGLShader::Vertex);
}

ShaderMgr::~ShaderMgr(){
    delete _fragShader;
    delete _vertShader;
    delete _ppfragShader;
    delete _ppvertShader;
}

bool ShaderMgr::compileSourceCode(ShaderType type, QString& code){
    bool result = false;
    switch(type){
    case ST_VERTEX:
        result = _vertShader->compileSourceCode(code.toLocal8Bit().data());
        break;
    case ST_FRAGMENT:
        result =_fragShader->compileSourceCode(code.toLocal8Bit().data());
        break;
    case ST_PP_VERTEX:
        result =_ppvertShader->compileSourceCode(code.toLocal8Bit().data());
        break;
    case ST_PP_FRAGMENT:
        result =_ppfragShader->compileSourceCode(code.toLocal8Bit().data());
        break;
    }

    return result;
}

bool ShaderMgr::compileSourceFile(ShaderType type, QString& file){
    bool result = false;
    switch(type){
    case ST_VERTEX:
        result =_vertShader->compileSourceFile(file);
        break;
    case ST_FRAGMENT:
        result =_fragShader->compileSourceFile(file);
        break;
    case ST_PP_VERTEX:
       result = _ppvertShader->compileSourceFile(file);
        break;
    case ST_PP_FRAGMENT:
        result =_ppfragShader->compileSourceFile(file);
        break;
    }

    return result;
}

bool ShaderMgr::link(ShaderGroup group){
    switch(group){
    case SG_GENERIC:
        _genericProgram.removeAllShaders();
        _genericProgram.addShader(_vertShader);
        _genericProgram.addShader(_fragShader);
        _genericProgram.link();
        break;
    case SG_POSTPRODUCTION:
        _postprodProgram.addShader(_ppvertShader);
        _postprodProgram.addShader(_ppfragShader);
        _postprodProgram.link();
        break;
    }
}

unsigned int ShaderMgr::getPPSamplerLocation(int index){
    if(index == 0)
        return _postprodProgram.uniformLocation("colorMap");
    else if(index == 1)
        return _postprodProgram.uniformLocation("depthMap");
    else
        return 0u;
}

void ShaderMgr::bindUniformVector(const QString& tag, float * values){
    GLuint location = _genericProgram.uniformLocation(tag);
    qDebug() << "location:" << location;

    _genericProgram.setUniformValue(tag.toStdString().c_str(),values[0],values[1],values[2]);
}

void ShaderMgr::getCompilationLog(ShaderType type, QString& log){
    log.clear();
    switch(type){
    case ST_VERTEX:
        if (_vertShader->isCompiled()) log.append("Shader compiled succesfully =)");
        else log.append(_vertShader->log());
        break;
    case ST_FRAGMENT:
        if (_fragShader->isCompiled()) log.append("Shader compiled succesfully =)");
        else log.append(_fragShader->log());
        break;
    case ST_PP_VERTEX:
        if (_ppvertShader->isCompiled()) log.append("Shader compiled succesfully =)");
        else log.append(_ppvertShader->log());
        break;
    case ST_PP_FRAGMENT:
        if (_ppfragShader->isCompiled()) log.append("Shader compiled succesfully =)");
        else log.append(_ppfragShader->log());
        break;
    }
}

void ShaderMgr::getCompilationLog(ShaderGroup type, QString& log){
    log.clear();
    switch(type){
    case SG_GENERIC:
        log = _genericProgram.log();
        break;
    case SG_POSTPRODUCTION:
        log = _postprodProgram.log();
        break;
    }
}

void ShaderMgr::useProgram(ShaderGroup group){
    switch(group){
    case SG_GENERIC:
        _genericProgram.bind();
        break;
    case SG_POSTPRODUCTION:
        _postprodProgram.bind();
        break;
    }
}

void ShaderMgr::releaseProgram(ShaderGroup group){
    switch(group){
    case SG_GENERIC:
        _genericProgram.release();
        break;
    case SG_POSTPRODUCTION:
        _postprodProgram.release();
        break;
    }
}

QString ShaderMgr::getShaderCode(ShaderType type){
    QByteArray code;
    switch(type){
    case ST_VERTEX:
        code = _vertShader->sourceCode();
        break;
    case ST_FRAGMENT:
        code = _fragShader->sourceCode();
        break;
    case ST_PP_VERTEX:
        code = _ppvertShader->sourceCode();
        break;
    case ST_PP_FRAGMENT:
        code = _ppfragShader->sourceCode();
        break;
    }

    return QString(code);
}
