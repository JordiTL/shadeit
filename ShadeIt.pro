#-------------------------------------------------
#
# Project created by QtCreator 2014-04-07T20:06:03
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ShadeIt
TEMPLATE = app
CONFIG += c++11

INCLUDEPATH += $$PWD/../libs/boost/includes
DEPENDPATH += $$PWD/../libs/boost/includes



SOURCES += main.cpp\
        mainwindow.cpp \
    wglholder.cpp \
    wglcanvas.cpp \
    wshadereditor.cpp \
    configmgr.cpp \
    glslhighlighter.cpp \
    gldebug.cpp \
    scene.cpp \
    shadermgr.cpp \
    loghighlighter.cpp \
    daboutus.cpp

HEADERS  += mainwindow.h \
    wglholder.h \
    wglcanvas.h \
    wshadereditor.h \
    configmgr.h \
    singleton.h \
    callonce.h \
    vector3.h \
    glslhighlighter.h \
    gldebug.h \
    scene.h \
    shadermgr.h \
    loghighlighter.h \
    daboutus.h

FORMS    += mainwindow.ui \
    wglholder.ui \
    wshadereditor.ui \
    daboutus.ui

RESOURCES += \
    resources.qrc

win32: LIBS += -L$$PWD/../libs/assimp/lib/ -llibassimp
win32: RC_ICONS += $$PWD/resources/images/app_icon.ico

INCLUDEPATH += $$PWD/../libs/assimp/includes
DEPENDPATH += $$PWD/../libs/assimp/includes
