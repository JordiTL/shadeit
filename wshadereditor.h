/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef WSHADEREDITOR_H
#define WSHADEREDITOR_H

#include <QWidget>
#include <QString>
#include "glslhighlighter.h"
#include "loghighlighter.h"

namespace Ui {
class WShaderEditor;
}

class WShaderEditor : public QWidget
{
    Q_OBJECT
public:
    static QString DEFAULT_VERTEX_SHADER;
    static QString DEFAULT_FRAGMENT_SHADER;
    static QString DEFAULT_PP_VERTEX_SHADER;
    static QString DEFAULT_PP_FRAGMENT_SHADER;

private:
    void _onCreation();
    bool _saveTextFileShader(const QString& path, const QString& code);

public:
    explicit WShaderEditor(QWidget *parent = 0);
    ~WShaderEditor();

private:
    Ui::WShaderEditor *ui;

    GLSLHighlighter * _vertHL;
    GLSLHighlighter * _fragHL;
    GLSLHighlighter * _ppvertHL;
    GLSLHighlighter * _ppfragHL;
    LogHighlighter * _logHL;

signals:
    void compileShadersAction();


public slots:
    void loadDefaultVertexShaderReaction();
    void loadDefaultFragmentShaderReaction();
    void loadDefaultPPVertexShaderReaction();
    void loadDefaultPPFragmentShaderReaction();

    void updateShaderLogReaction(const QString& log);
    void appendShaderLogReaction(const QString& log);

    void updateVertShaderViewerReaction(const QString& code);
    void updateFragShaderViewerReaction(const QString& code);
    void updatePPVertShaderViewerReaction(const QString& code);
    void updatePPFragShaderViewerReaction(const QString& code);

    void compileShadersReaction();

    void saveVertShaderReaction(const QString& filename);
    void saveFragShaderReaction(const QString& filename);
    void savePPVertShaderReaction(const QString& filename);
    void savePPFragShaderReaction(const QString& filename);

    void changeDescriptionReaction(int currentTab);
};

#endif // WSHADEREDITOR_H
