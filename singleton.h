/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef SINGLETON_H
#define SINGLETON_H
#include <QtCore/QtGlobal>
#include <QtCore/QScopedPointer>
#include "CallOnce.h"

template <class T>
class Singleton {
public:

    static T& instance() {
        qCallOnce(init, flag);
        return *tptr;
    }

    static void init() {
        tptr.reset(new T);
    }

private:

    Singleton() {
    };

    ~Singleton() {
    };
    Q_DISABLE_COPY(Singleton)

    static QScopedPointer<T> tptr;
    static QBasicAtomicInt flag;
};

template<class T> QScopedPointer<T> Singleton<T>::tptr(0);
template<class T> QBasicAtomicInt Singleton<T>::flag
= Q_BASIC_ATOMIC_INITIALIZER(CallOnce::CO_Request);



#endif	/* SINGLETON_H */
