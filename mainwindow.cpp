/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QString>
#include <QFileDialog>
#include <Qfile>
#include <QTextStream>
#include <QMessageBox>
#include "configmgr.h"
#include "singleton.h"
#include "daboutus.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::updateShaderLogReaction(const QString &log){
    emit updateShaderLogAction(log);
}

void MainWindow::updateVertShaderViewerReaction(const QString& code){
    emit updateVertShaderViewerAction(code);
}
void MainWindow::updateFragShaderViewerReaction(const QString& code){
    emit updateFragShaderViewerAction(code);
}
void MainWindow::updatePPVertShaderViewerReaction(const QString& code){
    emit updatePPVertShaderViewerAction(code);
}
void MainWindow::updatePPFragShaderViewerReaction(const QString& code){
    emit updatePPFragShaderViewerAction(code);
}

bool MainWindow::_loadTextFileShader(const QString& path, QString& code){
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox::information(0,"Error loading file",file.errorString());
        qDebug()<< "Error loading file: " << path;
        return false;
    }

    QTextStream in(&file);
    code = in.readAll();

    return true;
}



void MainWindow::openVertexShaderReaction(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Vertex Shader"), "", tr("Text Files (*.vert *.glsl *.txt)"));
    QString code;
    if(_loadTextFileShader(fileName, code))
        emit updateVertShaderViewerAction(code);
}

void MainWindow::openFragmentShaderReaction(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Fragment Shader"), "", tr("Text Files (*.frag *.glsl *.txt)"));
    QString code;
    if(_loadTextFileShader(fileName, code))
        emit updateFragShaderViewerAction(code);
}

void MainWindow::openPPVertexShaderReaction(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open PP Vertex Shader"), "", tr("Text Files (*.vert *.glsl *.txt)"));
    QString code;
    if(_loadTextFileShader(fileName, code))
        emit updatePPVertShaderViewerAction(code);
}

void MainWindow::openPPFragmentShaderReaction(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open PP Fragment Shader"), "", tr("Text Files (*.frag *.glsl *.txt)"));
    QString code;
    if(_loadTextFileShader(fileName, code))
        emit updatePPFragShaderViewerAction(code);
}

void MainWindow::saveVertShaderReaction(){
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save Vertex Shader"), "", tr("Text Files (*.vert *.glsl *.txt)"));
    emit saveVertShaderAction(fileName);
}

void MainWindow::saveFragShaderReaction(){
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save Fragment Shader"), "", tr("Text Files (*.frag *.glsl *.txt)"));
    emit saveFragShaderAction(fileName);
}

void MainWindow::savePPVertShaderReaction(){
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save PP Vertex Shader"), "", tr("Text Files (*.vert *.glsl *.txt)"));
    emit savePPVertShaderAction(fileName);
}

void MainWindow::savePPFragShaderReaction(){
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save PP Fragment Shader"), "", tr("Text Files (*.frag *.glsl *.txt)"));
    emit savePPFragShaderAction(fileName);
}

void MainWindow::openCustomSceneReaction(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Custom Mesh"), "", tr("Mesh Files (*.obj *.3ds)"));

    emit loadMeshAction(fileName);
}

void MainWindow::exitReaction(){
    this->close();
}

void MainWindow::loadDefaultsReaction(){
    Singleton<ConfigMgr>::instance().loadDefaults();

    //emit updateVertShaderViewerAction();
    //emit updateFragShaderViewerAction();
    //emit updatePPVertShaderViewerAction();
    //emit updatePPFragShaderViewerAction();

    emit updateGLAction();
}

void MainWindow::showAboutUsReaction(){
    daboutus dialog(this);
    dialog.exec();
}


void MainWindow::loadCubeReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.cubepath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadKnotReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.knotpath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadUvSphereReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.uvspherepath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadIcoSphereReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.icospherepath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadPlaneReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.planepath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadSuzanneReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.suzannepath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadBunnyReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.bunnypath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadLinkReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.linkpath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadTorusReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.toruspath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}

void MainWindow::loadTeapotReaction(){
    QString path(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.teapotpath").c_str());
    emit loadMeshAction(path);
    emit updateGLAction();
}


