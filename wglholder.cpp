/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "wglholder.h"
#include "ui_wglholder.h"
#include <QString>
#include <QDebug>
#include "singleton.h"
#include "configmgr.h"

WGLHolder::WGLHolder(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WGLHolder)
{
    ui->setupUi(this);
    _onCreation();
}

WGLHolder::~WGLHolder()
{
    delete ui;
}

void WGLHolder::_onCreation(){
    Singleton<ConfigMgr>::instance().loadDefaults();

    ConfigMgr& mgr = Singleton<ConfigMgr>::instance();
    ui->light0posx->setValue(mgr.queryPropertyAs<float>("render.light0positionX"));
    ui->light0posy->setValue(mgr.queryPropertyAs<float>("render.light0positionY"));
    ui->light0posz->setValue(mgr.queryPropertyAs<float>("render.light0positionZ"));
    ui->light0ambientr->setValue(mgr.queryPropertyAs<float>("render.light0ambientR"));
    ui->light0ambientg->setValue(mgr.queryPropertyAs<float>("render.light0ambientG"));
    ui->light0ambientb->setValue(mgr.queryPropertyAs<float>("render.light0ambientB"));
    ui->light0diffuser->setValue(mgr.queryPropertyAs<float>("render.light0diffuseR"));
    ui->light0diffuseg->setValue(mgr.queryPropertyAs<float>("render.light0diffuseG"));
    ui->light0diffuseb->setValue(mgr.queryPropertyAs<float>("render.light0diffuseB"));
    ui->light0specularr->setValue(mgr.queryPropertyAs<float>("render.light0specularR"));
    ui->light0specularg->setValue(mgr.queryPropertyAs<float>("render.light0specularG"));
    ui->light0specularb->setValue(mgr.queryPropertyAs<float>("render.light0specularB"));

    ui->light1positionx->setValue(mgr.queryPropertyAs<float>("render.light1positionX"));
    ui->light1positiony->setValue(mgr.queryPropertyAs<float>("render.light1positionY"));
    ui->light1positionz->setValue(mgr.queryPropertyAs<float>("render.light1positionZ"));
    ui->light1ambientr->setValue(mgr.queryPropertyAs<float>("render.light1ambientR"));
    ui->light1ambientg->setValue(mgr.queryPropertyAs<float>("render.light1ambientG"));
    ui->light1ambientb->setValue(mgr.queryPropertyAs<float>("render.light1ambientB"));
    ui->light1diffuser->setValue(mgr.queryPropertyAs<float>("render.light1diffuseR"));
    ui->light1diffuseg->setValue(mgr.queryPropertyAs<float>("render.light1diffuseG"));
    ui->light1diffuseb->setValue(mgr.queryPropertyAs<float>("render.light1diffuseB"));
    ui->light1specularr->setValue(mgr.queryPropertyAs<float>("render.light1specularR"));
    ui->light1specularg->setValue(mgr.queryPropertyAs<float>("render.light1specularG"));
    ui->light1specularb->setValue(mgr.queryPropertyAs<float>("render.light1specularB"));

    ui->materialambientr->setValue(mgr.queryPropertyAs<float>("render.materialambientR"));
    ui->materialambientg->setValue(mgr.queryPropertyAs<float>("render.materialambientG"));
    ui->materialambientb->setValue(mgr.queryPropertyAs<float>("render.materialambientB"));
    ui->materialdiffuser->setValue(mgr.queryPropertyAs<float>("render.materialdiffuseR"));
    ui->materialdiffuseg->setValue(mgr.queryPropertyAs<float>("render.materialdiffuseG"));
    ui->materialdiffuseb->setValue(mgr.queryPropertyAs<float>("render.materialdiffuseB"));
    ui->materialspecularr->setValue(mgr.queryPropertyAs<float>("render.materialspecularR"));
    ui->materialspecularg->setValue(mgr.queryPropertyAs<float>("render.materialspecularG"));
    ui->materialspecularb->setValue(mgr.queryPropertyAs<float>("render.materialspecularB"));

    ui->materialshininess->setValue(mgr.queryPropertyAs<float>("render.materialshininess"));
}

void WGLHolder::noMaterialToggleReaction(bool value){
    std::string tag("display.nomaterial");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);

    emit updateGLAction();
}

void WGLHolder::wireframeToggleReaction(bool value){
    std::string tag("display.wireframe");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);

    emit updateGLAction();
}

void WGLHolder::multisamplingToggleReaction(bool value){
    std::string tag("display.multisampling");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);

    emit updateGLAction();
}

void WGLHolder::modelRotationToggleReaction(bool value){
    std::string tag("display.modelRotation");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);

    emit updateGLAction();
}

void WGLHolder::lightsRotationToggleReaction(bool value){
    std::string tag("display.lightsRotation");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);

    emit updateGLAction();
}

void WGLHolder::twoLightsToggleReaction(bool value){
    std::string tag("display.twolights");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);

    emit updateGLAction();
}

void WGLHolder::updateShaderLogReaction(const QString &log){
    emit updateShaderLogAction(log);
}

void WGLHolder::updateVertShaderViewerReaction(const QString& code){
    emit updateVertShaderViewerAction(code);
}

void WGLHolder::updateFragShaderViewerReaction(const QString& code){
    emit updateFragShaderViewerAction(code);
}

void WGLHolder::updatePPVertShaderViewerReaction(const QString& code){
    emit updatePPVertShaderViewerAction(code);
}

void WGLHolder::updatePPFragShaderViewerReaction(const QString& code){
    emit updatePPFragShaderViewerAction(code);
}

void WGLHolder::compileShadersReaction(){
    emit compileShadersAction();
}

void WGLHolder::updateLight0PositionXReaction(double value){
    std::string tag("render.light0positionX");
    ValueProxy proxy(value);

    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0PositionYReaction(double value){
    std::string tag("render.light0positionY");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0PositionZReaction(double value){
    std::string tag("render.light0positionZ");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0AmbientRReaction(double value){
    std::string tag("render.light0ambientR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0AmbientGReaction(double value){
    std::string tag("render.light0ambientG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0AmbientBReaction(double value){
    std::string tag("render.light0ambientB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateLight0DiffuseRReaction(double value){
    std::string tag("render.light0diffuseR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0DiffuseGReaction(double value){
    std::string tag("render.light0diffuseG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0DiffuseBReaction(double value){
    std::string tag("render.light0diffuseB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateLight0SpecularRReaction(double value){
    std::string tag("render.light0specularR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0SpecularGReaction(double value){
    std::string tag("render.light0specularG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight0SpecularBReaction(double value){
    std::string tag("render.light0specularB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateLight1PositionXReaction(double value){
    std::string tag("render.light1positionX");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1PositionYReaction(double value){
    std::string tag("render.light1positionY");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1PositionZReaction(double value){
    std::string tag("render.light1positionZ");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateLight1AmbientRReaction(double value){
    std::string tag("render.light1ambientR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1AmbientGReaction(double value){
    std::string tag("render.light1ambientG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1AmbientBReaction(double value){
    std::string tag("render.light1ambientB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1DiffuseRReaction(double value){
    std::string tag("render.light1diffuseR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1DiffuseGReaction(double value){
    std::string tag("render.light1diffuseG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1DiffuseBReaction(double value){
    std::string tag("render.light1diffuseB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateLight1SpecularRReaction(double value){
    std::string tag("render.light1specularR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1SpecularGReaction(double value){
    std::string tag("render.light1specularG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateLight1SpecularBReaction(double value){
    std::string tag("render.light1specularB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateMaterialAmbientRReaction(double value){
    std::string tag("render.materialambientR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialAmbientGReaction(double value){
    std::string tag("render.materialambientG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialAmbientBReaction(double value){
    std::string tag("render.materialambientB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateMaterialDiffuseRReaction(double value){
    std::string tag("render.materialdiffuseR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialDiffuseGReaction(double value){
    std::string tag("render.materialdiffuseG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialDiffuseBReaction(double value){
    std::string tag("render.materialdiffuseB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}


void WGLHolder::updateMaterialSpecularRReaction(double value){
    std::string tag("render.materialspecularR");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialSpecularGReaction(double value){
    std::string tag("render.materialspecularG");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialSpecularBReaction(double value){
    std::string tag("render.materialspecularB");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateMaterialShininessReaction(double value){
    std::string tag("render.materialshininess");
    ValueProxy proxy(value);
    Singleton<ConfigMgr>::instance().setProperty(tag,proxy);
    emit updateGLAction();
}

void WGLHolder::updateGLReaction(){
    emit updateGLAction();
}

void WGLHolder::loadMeshReaction(const QString& filename){
    emit loadMeshAction(filename);
}

