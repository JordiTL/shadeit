/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "scene.h"
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/material.h>
#include <QDebug>
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include "gldebug.h"
#include <QGLFunctions>
#include "singleton.h"
#include "configmgr.h"

#define VERTEX_COMPONENTS 3
#define FACE_VERTICES 3



Scene::Scene(){

}

Scene::~Scene(){
}

bool Scene::loadFromFile(const std::string& filename, QGLWidget * widgetContex){
    QFile file(filename.c_str());

    if(!file.exists())
        return false;

    QFileInfo fileInfo(file);

    Assimp::Importer importer;


    const aiScene * scene = importer.ReadFile( filename,
        aiProcess_CalcTangentSpace       |
        aiProcess_Triangulate            |
        aiProcess_JoinIdenticalVertices  |
        aiProcess_SortByPType);

    std::cerr << importer.GetErrorString() << std::endl;

    if(scene == nullptr){
        std::cerr << "Couldn't load model. Error Importing Asset: "<< filename << std::endl;
        return false;
    }

    _loadTextures(scene, widgetContex, fileInfo.absoluteDir().path());

    for(unsigned int i = 0; i < scene->mNumMeshes; ++i){
        aiMesh* mesh = scene->mMeshes[i];
        MeshData mdata;
        mdata._vertexCount = mesh->mNumFaces*3;
        mdata._vertexArray = new float[mesh->mNumFaces*3*3];
        mdata._normalArray = new float[mesh->mNumFaces*3*3];
        mdata._uvArray = new float[mesh->mNumFaces*3*2];
        mdata._colorArray = new float[mesh->mNumFaces*3*4];

        // For each face (triangle) in the mesh
        for(unsigned int j = 0; j < mesh->mNumFaces; ++j){
            const aiFace& face = mesh->mFaces[j];

            for(unsigned int k = 0; k < 3; ++k){
                aiVector3D pos = mesh->mVertices[face.mIndices[k]];
                aiVector3D uv = mesh->GetNumUVChannels() > 0 ? mesh->mTextureCoords[0][face.mIndices[k]] : aiVector3D(1.0f, 1.0f, 1.0f);
                aiVector3D normal = mesh->HasNormals() ? mesh->mNormals[face.mIndices[k]] : aiVector3D(1.0f, 1.0f, 1.0f);
                aiColor4D color = mesh->HasVertexColors(0u) ? *mesh->mColors[face.mIndices[k]] : aiColor4D(.5f,.5f,.5f, 1.f);

                memcpy(mdata._uvArray,&uv,sizeof(float)*2);
                mdata._uvArray+=2;

                memcpy(mdata._normalArray,&normal,sizeof(float)*3);
                mdata._normalArray+=3;

                memcpy(mdata._vertexArray,&pos,sizeof(float)*3);
                mdata._vertexArray+=3;

                memcpy(mdata._colorArray,&color,sizeof(float)*4);
                mdata._colorArray+=4;
            }
        }

        mdata._uvArray-=mesh->mNumFaces*3*2;
        mdata._normalArray-=mesh->mNumFaces*3*3;
        mdata._vertexArray-=mesh->mNumFaces*3*3;
        mdata._colorArray-=mesh->mNumFaces*3*4;
        try{
        mdata._textureId = _textures.at(mesh->mMaterialIndex);
        }catch(std::out_of_range ex){
            Q_UNUSED(ex);
            mdata._textureId = 0;
        }

        _meshes.push_back(mdata);
    }

    return true;
}

void Scene::_loadTextures(const aiScene* scene,QGLWidget * widgetContex, const QString& pathPrefix){
    /* scan scene's materials for textures */
    for (unsigned int m=0; m < scene->mNumMaterials; ++m){
        int texIndex = 0; // only frist texture
        aiString path;  // filename

        aiReturn texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
        //qDebug() << "Loading texture: " << path.data;

        QString fullPath(pathPrefix);
        fullPath.append("/");
        fullPath.append(path.data);

        if(texFound == AI_SUCCESS) {
            //fill map with textures, OpenGL image ids set to 0

            QPixmap tex(fullPath);
            if(!tex.isNull()){
            _textures.insert(std::pair<unsigned int, unsigned int>(m,widgetContex->bindTexture(tex, GL_TEXTURE_2D)));
            //_texturesData.push_back(tex);
            qDebug() << "Loading texture succesful: " << fullPath;
            }else{
                qDebug() << "Loading texture error: " << fullPath;
            }
        }else
            qDebug() << "Texture not found: " << fullPath;
    }
}

void Scene::render(){
    for(unsigned int i = 0; i < _meshes.size(); ++i){
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);

        glVertexPointer(3,GL_FLOAT,0,_meshes.at(i)._vertexArray);
        glNormalPointer(GL_FLOAT,0,_meshes.at(i)._normalArray);

        if(!(Singleton<ConfigMgr>::instance().queryPropertyAs<bool>("display.nomaterial")))
            glBindTexture(GL_TEXTURE_2D, _meshes.at(i)._textureId);
        else
            glBindTexture(GL_TEXTURE_2D,0);
        //glClientActiveTextureARB(GL_TEXTURE0);
        glTexCoordPointer(2,GL_FLOAT,0,_meshes.at(i)._uvArray);
        glColorPointer(4,GL_FLOAT,0,_meshes.at(i)._colorArray);

        glDrawArrays(GL_TRIANGLES,0,_meshes.at(i)._vertexCount);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glBindTexture(GL_TEXTURE_2D,0);
    }
}

 void Scene::reset(QGLWidget * widgetContex){
     for(std::map<unsigned int, unsigned int>::const_iterator it = _textures.cbegin();
         it != _textures.cend();
         ++it){
         widgetContex->deleteTexture(it->second);
     }

     _textures.clear();

     for(unsigned int i = 0; i < _texturesData.size(); ++i){
         delete _texturesData[i];
     }

     _texturesData.clear();

     for(unsigned int i = 0; i < _meshes.size(); ++i){
         delete _meshes[i]._vertexArray;
         delete _meshes[i]._normalArray;
         delete _meshes[i]._uvArray;
         delete _meshes[i]._colorArray;
     }
     _meshes.clear();
 }
