/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef CONFIGMGR_H
#define CONFIGMGR_H

#include <QObject>
#include <QMutex>
#include <boost/lexical_cast.hpp>
#include <string>
#include <unordered_map>



class ValueProxy{
private:
    std::string _any;

public:
    // Only allow primitive types by constructor
    ValueProxy(const short value){
        _any = boost::lexical_cast<std::string>(value);
    }
    ValueProxy(const int value){
        _any = boost::lexical_cast<std::string>(value);
    }
    ValueProxy(const unsigned int value){
        _any = boost::lexical_cast<std::string>(value);
    }
    ValueProxy(const long value){
        _any = boost::lexical_cast<std::string>(value);
    }
    ValueProxy(const float value){
        _any = boost::lexical_cast<std::string>(value);
    }
    ValueProxy(const double value){
        _any = boost::lexical_cast<std::string>(value);
    }

    ValueProxy(const std::string& value) : _any(value) {}



    ValueProxy(const ValueProxy& value) {
        _any = value._any;
    }

    template<typename T>
    T get(){
        return boost::lexical_cast<T>(_any);
    }
};

class ConfigMgr : public QObject{
    Q_OBJECT

private:
    typedef std::unordered_map<std::string, ValueProxy> PropertyMap;
    typedef std::pair<std::string, ValueProxy> PropertyEntry;

private:
    PropertyMap _propertyMap;
    PropertyMap _defaultPropertyMap;
    QMutex _mutex;

public:
    explicit ConfigMgr(QObject *parent = 0);
    virtual ~ConfigMgr();

    void loadDefaults();

    template<typename T>
    T queryPropertyAs(const std::string& key);
    ValueProxy queryProperty(const std::string& key);

    bool setProperty(const std::string& key, ValueProxy& value);
};

template<typename T>
T ConfigMgr::queryPropertyAs(const std::string& key){
    return _propertyMap.at(key).get<T>();
}


#endif // CONFIGMGR_H
