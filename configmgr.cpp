/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "configmgr.h"
#include <QMutexLocker>
#include <boost/foreach.hpp>
#include <iostream>
#include <exception>


ConfigMgr::ConfigMgr(QObject *parent) : QObject(parent){
    loadDefaults();
}

ConfigMgr::~ConfigMgr() {
}

void ConfigMgr::loadDefaults() {
    QMutexLocker locker(&_mutex);
    Q_UNUSED(locker);

    _defaultPropertyMap.clear();
    // HARD CODED - IMPORTANT

    _defaultPropertyMap.insert(PropertyEntry("display.nomaterial", ValueProxy(false)));
    _defaultPropertyMap.insert(PropertyEntry("display.wireframe", ValueProxy(false)));
    _defaultPropertyMap.insert(PropertyEntry("display.multisampling", ValueProxy(true)));
    _defaultPropertyMap.insert(PropertyEntry("display.modelRotation", ValueProxy(false)));
    _defaultPropertyMap.insert(PropertyEntry("display.lightsRotation", ValueProxy(false)));
    _defaultPropertyMap.insert(PropertyEntry("display.twolights", ValueProxy(false)));

    _defaultPropertyMap.insert(PropertyEntry("render.light0positionX", ValueProxy(5.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0positionY", ValueProxy(5.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0positionZ", ValueProxy(5.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0ambientR", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0ambientG", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0ambientB", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0diffuseR", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0diffuseG", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0diffuseB", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0specularR", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0specularG", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light0specularB", ValueProxy(1.0)));

    _defaultPropertyMap.insert(PropertyEntry("render.light1positionX", ValueProxy(-5.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1positionY", ValueProxy(-5.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1positionZ", ValueProxy(-5.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1ambientR", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1ambientG", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1ambientB", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1diffuseR", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1diffuseG", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1diffuseB", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1specularR", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1specularG", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.light1specularB", ValueProxy(1.0)));

    _defaultPropertyMap.insert(PropertyEntry("render.materialambientR", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialambientG", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialambientB", ValueProxy(0.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialdiffuseR", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialdiffuseG", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialdiffuseB", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialspecularR", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialspecularG", ValueProxy(1.0)));
    _defaultPropertyMap.insert(PropertyEntry("render.materialspecularB", ValueProxy(1.0)));

    _defaultPropertyMap.insert(PropertyEntry("render.materialshininess", ValueProxy(50.0)));

    _defaultPropertyMap.insert(PropertyEntry("shader.defaultfrag", ValueProxy("resources/shaders/default.frag")));
    _defaultPropertyMap.insert(PropertyEntry("shader.defaultvert", ValueProxy("resources/shaders/default.vert")));
    _defaultPropertyMap.insert(PropertyEntry("shader.defaultfragPP", ValueProxy("resources/shaders/defaultPP.frag")));
    _defaultPropertyMap.insert(PropertyEntry("shader.defaultvertPP", ValueProxy("resources/shaders/defaultPP.vert")));

    _defaultPropertyMap.insert(PropertyEntry("preview.cubepath", ValueProxy("resources/models/default/cube.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.knotpath", ValueProxy("resources/models/default/knot.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.icospherepath", ValueProxy("resources/models/default/icosphere.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.uvspherepath", ValueProxy("resources/models/default/uvsphere.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.linkpath", ValueProxy("resources/models/default/link.obj")));
    _defaultPropertyMap.insert(PropertyEntry("preview.teapotpath", ValueProxy("resources/models/default/utahTeapot.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.bunnypath", ValueProxy("resources/models/default/standfordBunny.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.planepath", ValueProxy("resources/models/default/plane.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.toruspath", ValueProxy("resources/models/default/torus.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.uvspherepath", ValueProxy("resources/models/default/uvsphere.ply")));
    _defaultPropertyMap.insert(PropertyEntry("preview.suzannepath", ValueProxy("resources/models/default/suzanne.ply")));




    _propertyMap.clear();
    _propertyMap.insert(_defaultPropertyMap.begin(), _defaultPropertyMap.end());

}

ValueProxy ConfigMgr::queryProperty(const std::string& key) {
    // No default CTOR
    ValueProxy value("");

    try {
        value = _propertyMap.at(key);
    } catch (std::out_of_range ex) {
        value = _defaultPropertyMap.at(key);
    }

    return value;
}

bool ConfigMgr::setProperty(const std::string& key, ValueProxy& value) {
    QMutexLocker locker(&_mutex);
    Q_UNUSED(locker);

    PropertyMap::iterator it = _propertyMap.find(key);

    // No overlapped key
    if (it == _propertyMap.end()){
        std::cerr << "Trying to set an unknow property." << std::endl;

        return false;
    }


    // Overlapped key
    _propertyMap.erase(it);
    _propertyMap.insert(PropertyEntry(key, value));

    return true;
}
