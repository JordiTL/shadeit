/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "wglcanvas.h"
#include <QWidget>
#include <GL/glu.h>
#include <string>
#include <QString>
#include <QDebug>
#include <QGLFunctions>
#include <QOpenGLContext>
#include <QOpenGLDebugLogger>
#include <QMouseEvent>
#include <cmath>
#include <QGLFrameBufferObject>
#include <QTimer>
#include <QGLContext>
#include "singleton.h"
#include "configmgr.h"
#include "scene.h"
#include "gldebug.h"
#include "shadermgr.h"

#define PI 3.14159265358979323846

#define OFFLINE_RENDER_W 2000
#define OFFLINE_RENDER_H 2000

WGLCanvas::WGLCanvas(QWidget *parent) :
    QGLWidget( parent){
    format().setVersion(3,0);
    format().setDepth(true);

    //setFormat(QGLFormat(QGL::Rgba | QGL::DoubleBuffer | QGL::DepthBuffer));
    _mesh = new Scene();

    _cam = new Camera3D();
    _mouse = new MouseState();
    _sceneState = new SceneState();

    makeCurrent();

    _scapeColor = parent->palette().color(QWidget::backgroundRole());


    setMouseTracking(true);

    _modelRotTimer = new QTimer(this);

    _modelRotTimer->setInterval(50);

    QObject::connect(_modelRotTimer, SIGNAL(timeout()), this, SLOT(autoRotateModelReaction()));

    _modelRotAngle=0.f;
    _lightsRotAngle=0.f;

    _modelRotTimer->start();

}

WGLCanvas::~WGLCanvas(){
    glDeleteTextures(1,&_glInfo.img);
    glDeleteTextures(1,&_glInfo.imgDepth);

    _glFunc.glDeleteBuffers(1, &_glInfo.rbo);
    _glFunc.glDeleteFramebuffers(1, &_glInfo.fbo);

    delete _mesh;
    delete _cam;
    delete _mouse;
    delete _sceneState;

    delete _modelRotTimer;
}

void WGLCanvas::resizeGL(int width, int height)
{
    // Compute aspect ratio of the new window
    if (height == 0) height = 1;                // To prevent divide by 0
    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    // Set the viewport to cover the new window
    glViewport(0, 0, width, height);

    // Set the aspect ratio of the clipping volume to match the viewport
    glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
    glLoadIdentity();             // Reset
    // Enable perspective projection with fovy, aspect, zNear and zFar
    gluPerspective(45.0f, aspect, 0.1f, 100.0f);
}

void WGLCanvas::initializeGL()
{

    // ----------- INIT OPENGL RAW FUNCTIONS
    _glFunc.initializeGLFunctions(this->context());

    // ----------- CREATING OFFLINE "RENDER TO" TEXTURE
    glGenTextures(1, &_glInfo.img);
    glBindTexture(GL_TEXTURE_2D, _glInfo.img);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, OFFLINE_RENDER_W, OFFLINE_RENDER_H, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0); // place multisampling here too!
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &_glInfo.imgDepth);
    glBindTexture(GL_TEXTURE_2D, _glInfo.imgDepth);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, OFFLINE_RENDER_W, OFFLINE_RENDER_H, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glBindTexture(GL_TEXTURE_2D, 0);


    // ----------- DEPTH BUFFER FOR THE NEW FBO

    _glFunc.glGenRenderbuffers(1, &_glInfo.rbo);
    _glFunc.glBindRenderbuffer(GL_RENDERBUFFER, _glInfo.rbo);
    _glFunc.glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, OFFLINE_RENDER_W, OFFLINE_RENDER_H);
    _glFunc.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _glInfo.rbo);
    _glFunc.glBindRenderbuffer(GL_RENDERBUFFER, 0);


    // ----------- CREATING FBO TO LINK ALL
    _glFunc.glGenFramebuffers(1, &_glInfo.fbo);
    _glFunc.glBindFramebuffer(GL_FRAMEBUFFER, _glInfo.fbo);
    _glFunc.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _glInfo.img, 0);
    _glFunc.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _glInfo.imgDepth, 0);



    GLuint status = _glFunc.glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status == GL_FRAMEBUFFER_COMPLETE)
        qDebug() << "PostProcessing effect initializing DONE\n";
    else
        qDebug() << "PostProcessing effect initializing ERROR!! \n";


    _glFunc.glBindFramebuffer(GL_FRAMEBUFFER, 0);


    // ----------- GENERAL SETTINGS
    GLfloat aspect = (GLfloat)width() / (GLfloat)height();
    gluPerspective(45.0f, aspect, 0.01f, 100.0f);

    qglClearColor(_scapeColor);
    glClearDepth(1.0f);                   // Set background depth to farthest
    glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
    glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel(GL_SMOOTH);   // Enable smooth shading
    glDisable(GL_MULTISAMPLE);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
    glViewport(0, 0, OFFLINE_RENDER_W, OFFLINE_RENDER_H);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);


    // ----------- LOADING DEFAULT SHADERS
    QString fragPath(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("shader.defaultfrag").c_str());
    QString vertPath(QString(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("shader.defaultvert").c_str()));

    QString fragPPPath(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("shader.defaultfragPP").c_str());
    QString vertPPPath(QString(Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("shader.defaultvertPP").c_str()));

    Singleton<ShaderMgr>::instance().compileSourceFile(ShaderMgr::ST_VERTEX, vertPath);
    Singleton<ShaderMgr>::instance().compileSourceFile(ShaderMgr::ST_FRAGMENT, fragPath);
    Singleton<ShaderMgr>::instance().link(ShaderMgr::SG_GENERIC);

    Singleton<ShaderMgr>::instance().compileSourceFile(ShaderMgr::ST_PP_VERTEX, vertPPPath);
    Singleton<ShaderMgr>::instance().compileSourceFile(ShaderMgr::ST_PP_FRAGMENT, fragPPPath);
    Singleton<ShaderMgr>::instance().link(ShaderMgr::SG_POSTPRODUCTION);

    QString vertCode(Singleton<ShaderMgr>::instance().getShaderCode(ShaderMgr::ST_VERTEX));
    QString fragCode(Singleton<ShaderMgr>::instance().getShaderCode(ShaderMgr::ST_FRAGMENT));

    QString vertPPCode(Singleton<ShaderMgr>::instance().getShaderCode(ShaderMgr::ST_PP_VERTEX));
    QString fragPPCode(Singleton<ShaderMgr>::instance().getShaderCode(ShaderMgr::ST_PP_FRAGMENT));

    emit updateVertShaderViewerAction(vertCode);
    emit updateFragShaderViewerAction(fragCode);

    emit updatePPVertShaderViewerAction(vertPPCode);
    emit updatePPFragShaderViewerAction(fragPPCode);

    // ----------- LOADING SCENE
     std::string path = Singleton<ConfigMgr>::instance().queryPropertyAs<std::string>("preview.knotpath");
    _mesh->loadFromFile(path, this);
}

void WGLCanvas::paintGL(){
    makeCurrent();

    // ----------- CONFIGURING TEX SETTINGS
    glBindTexture(GL_TEXTURE_2D, _glInfo.img);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glHint(GL_GENERATE_MIPMAP_HINT, GL_FASTEST);
    glBindTexture(GL_TEXTURE_2D, 0);

    glViewport(0,0,OFFLINE_RENDER_W,OFFLINE_RENDER_H);


    // ----------- RENDER TO OFFLINE FBO
    _glFunc.glBindFramebuffer(GL_FRAMEBUFFER, _glInfo.fbo);
    update();


    Singleton<ShaderMgr>::instance().useProgram(ShaderMgr::SG_GENERIC);


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
    // DEFAULT MODE
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_AMBIENT, _sceneState->mLight0Ambient);     // Setup ambient light
    glLightfv(GL_LIGHT0, GL_DIFFUSE, _sceneState->mLight0Diffuse);     // Setup diffuse light
    glLightfv(GL_LIGHT0, GL_SPECULAR, _sceneState->mLight0Specular);   // Setup specular light
    glLightfv(GL_LIGHT0, GL_POSITION, _sceneState->mLight0Position);

    glLightfv(GL_LIGHT1, GL_AMBIENT, _sceneState->mLight1Ambient);     // Setup ambient light
    glLightfv(GL_LIGHT1, GL_DIFFUSE, _sceneState->mLight1Diffuse);     // Setup diffuse light
    glLightfv(GL_LIGHT1, GL_SPECULAR, _sceneState->mLight1Specular);   // Setup specular light
    glLightfv(GL_LIGHT1, GL_POSITION, _sceneState->mLight1Position);

    glMaterialfv(GL_FRONT, GL_AMBIENT, _sceneState->mMaterialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, _sceneState->mMaterialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, _sceneState->mMaterialSpecular);
    glMaterialf(GL_FRONT, GL_SHININESS, _sceneState->mMaterialShininess);

    glTranslated(_cam->mCamLookAt.x,_cam->mCamLookAt.y,_cam->mCamLookAt.z);
    glRotated(_cam->mCamPos.x, 0.0,1.0,0.0);
    glRotated(_cam->mCamPos.y, 1.0,0.0,0.0);

    _mesh->render();
    //_glFunc.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _glInfo.imgDepth, 0);
    Singleton<ShaderMgr>::instance().releaseProgram(ShaderMgr::SG_GENERIC);

    //_frameBuffer->toImage().save("image.jpg");
    //_frameBuffer->release();
    _glFunc.glBindFramebuffer(GL_FRAMEBUFFER,0);



    // ------------ RENDER TO ONLINE SCREEN
    Singleton<ShaderMgr>::instance().useProgram(ShaderMgr::SG_POSTPRODUCTION);
    const GLuint colorMapLoc = Singleton<ShaderMgr>::instance().getPPSamplerLocation(0);
    const GLuint depthMapLoc = Singleton<ShaderMgr>::instance().getPPSamplerLocation(1);

    _glFunc.glUniform1i(colorMapLoc, 0);
    _glFunc.glUniform1i(depthMapLoc, 1);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



    glViewport(0,0,width(), height());

    glPushAttrib( GL_TEXTURE_BIT | GL_DEPTH_TEST | GL_LIGHTING );
    glDisable( GL_LIGHTING );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, OFFLINE_RENDER_W, OFFLINE_RENDER_H ,0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    _glFunc.glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _glInfo.img);
    glEnable(GL_TEXTURE_2D);
    _glFunc.glGenerateMipmap(GL_TEXTURE_2D);

    _glFunc.glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, _glInfo.imgDepth);
    glEnable(GL_TEXTURE_2D);

    glBegin(GL_QUADS);
    glColor3f( 1.0f, 0.0f, 0.0f );
    glTexCoord2f(0.0f,0.0f);   glVertex2f(0.0f,OFFLINE_RENDER_H);
    glColor3f( 0.0f, 1.0f, 0.0f );
    glTexCoord2f(1.0f,0.0f);   glVertex2f(OFFLINE_RENDER_W,OFFLINE_RENDER_H);
    glColor3f( 0.0f, 0.0f, 1.0f );
    glTexCoord2f(1.0f,1.0f);   glVertex2f(OFFLINE_RENDER_W,0.0f);
    glColor3f( 1.0f, 0.0f, 1.0f );
    glTexCoord2f(0.0f,1.0f);   glVertex2f(0.0f,0.0f);
    glEnd();

    glMatrixMode( GL_PROJECTION );
    glPopMatrix();

    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();

    glPopAttrib();
    Singleton<ShaderMgr>::instance().releaseProgram(ShaderMgr::SG_POSTPRODUCTION);
}

void WGLCanvas::update(){

    ConfigMgr& mgr = Singleton<ConfigMgr>::instance();

    if(!mgr.queryPropertyAs<bool>("display.lightsRotation")){
        _sceneState->mLight0Position[0] = mgr.queryPropertyAs<float>("render.light0positionX");
        _sceneState->mLight0Position[1] = mgr.queryPropertyAs<float>("render.light0positionY");
        _sceneState->mLight0Position[2] = mgr.queryPropertyAs<float>("render.light0positionZ");
    }
    _sceneState->mLight0Ambient[0] = mgr.queryPropertyAs<float>("render.light0ambientR");
    _sceneState->mLight0Ambient[1] = mgr.queryPropertyAs<float>("render.light0ambientG");
    _sceneState->mLight0Ambient[2] = mgr.queryPropertyAs<float>("render.light0ambientB");
    _sceneState->mLight0Diffuse[0] = mgr.queryPropertyAs<float>("render.light0diffuseR");
    _sceneState->mLight0Diffuse[1] = mgr.queryPropertyAs<float>("render.light0diffuseG");
    _sceneState->mLight0Diffuse[2] = mgr.queryPropertyAs<float>("render.light0diffuseB");
    _sceneState->mLight0Specular[0] = mgr.queryPropertyAs<float>("render.light0specularR");
    _sceneState->mLight0Specular[1] = mgr.queryPropertyAs<float>("render.light0specularG");
    _sceneState->mLight0Specular[2] = mgr.queryPropertyAs<float>("render.light0specularB");

    if(!mgr.queryPropertyAs<bool>("display.lightsRotation")){
        _sceneState->mLight1Position[0] = mgr.queryPropertyAs<float>("render.light1positionX");
        _sceneState->mLight1Position[1] = mgr.queryPropertyAs<float>("render.light1positionY");
        _sceneState->mLight1Position[2] = mgr.queryPropertyAs<float>("render.light1positionZ");
    }
    _sceneState->mLight1Ambient[0] = mgr.queryPropertyAs<float>("render.light1ambientR");
    _sceneState->mLight1Ambient[1] = mgr.queryPropertyAs<float>("render.light1ambientG");
    _sceneState->mLight1Ambient[2] = mgr.queryPropertyAs<float>("render.light1ambientB");
    _sceneState->mLight1Diffuse[0] = mgr.queryPropertyAs<float>("render.light1diffuseR");
    _sceneState->mLight1Diffuse[1] = mgr.queryPropertyAs<float>("render.light1diffuseG");
    _sceneState->mLight1Diffuse[2] = mgr.queryPropertyAs<float>("render.light1diffuseB");
    _sceneState->mLight1Specular[0] = mgr.queryPropertyAs<float>("render.light1specularR");
    _sceneState->mLight1Specular[1] = mgr.queryPropertyAs<float>("render.light1specularG");
    _sceneState->mLight1Specular[2] = mgr.queryPropertyAs<float>("render.light1specularB");

    _sceneState->mMaterialAmbient[0] = mgr.queryPropertyAs<float>("render.materialambientR");
    _sceneState->mMaterialAmbient[1] = mgr.queryPropertyAs<float>("render.materialambientG");
    _sceneState->mMaterialAmbient[2] = mgr.queryPropertyAs<float>("render.materialambientB");
    _sceneState->mMaterialDiffuse[0] = mgr.queryPropertyAs<float>("render.materialdiffuseR");
    _sceneState->mMaterialDiffuse[1] = mgr.queryPropertyAs<float>("render.materialdiffuseG");
    _sceneState->mMaterialDiffuse[2] = mgr.queryPropertyAs<float>("render.materialdiffuseB");
    _sceneState->mMaterialSpecular[0] = mgr.queryPropertyAs<float>("render.materialspecularR");
    _sceneState->mMaterialSpecular[1] = mgr.queryPropertyAs<float>("render.materialspecularG");
    _sceneState->mMaterialSpecular[2] = mgr.queryPropertyAs<float>("render.materialspecularB");

    _sceneState->mMaterialShininess = mgr.queryPropertyAs<float>("render.materialshininess");


    if( mgr.queryPropertyAs<bool>("display.wireframe"))
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    else
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    if( mgr.queryPropertyAs<bool>("display.multisampling"))
        glEnable( GL_MULTISAMPLE );
    else
        glDisable( GL_MULTISAMPLE );
}

void WGLCanvas::useShaderReaction(QGLShaderProgram& program){

}

void WGLCanvas::glMessageLoggedReaction(const QOpenGLDebugMessage &msg){
    qDebug() << msg.message();
}

void WGLCanvas::mouseMoveEvent(QMouseEvent *_event){
    if(!_mouse->mInitialized){
        _mouse->mLastPos.x = _event->pos().x();
        _mouse->mLastPos.y = _event->pos().y();
        _mouse->mInitialized = true;
    }

    if (this->rect().contains(_event->pos())) {
        //qDebug() << "IN" ;
        if(_event->buttons() & Qt::LeftButton){
            _cam->mCamLookAt.x -= (_mouse->mLastPos.x - _event->pos().x())*0.001;
            _cam->mCamLookAt.y += (_mouse->mLastPos.y - _event->pos().y())*0.001;

            //qDebug() <<"LOOKING AT x: " <<_cam->mCamLookAt.x  << "y: " <<_cam->mCamLookAt.y;

            _mouse->mLastPos.x = _event->pos().x();
            _mouse->mLastPos.y = _event->pos().y();
            updateGL();
        }else if(_event->buttons() & Qt::RightButton){

            _cam->mCamPos.x = -(_mouse->mLastPos.x - _event->pos().x());
            _cam->mCamPos.y = -(_mouse->mLastPos.y - _event->pos().y());
            //qDebug() <<"POSITIONATED AT x: " <<_cam->mCamPos.x  << "y: " <<_cam->mCamPos.z;
            updateGL();
        }
        else{
            _mouse->mInitialized = false;
        }
    }
    else {
        //qDebug() << "OUT" ;
        _mouse->mInitialized = false;
    }

    if(_event->buttons() & Qt::NoButton){
        _mouse->mInitialized = false;
    }


}

void WGLCanvas::wheelEvent(QWheelEvent * event){
    _cam->mCamLookAt.z+=(static_cast<double>(event->delta())/120.0);
    //qDebug() <<"ZOOM FACTOR: " << _cam->mCamLookAt.z ;
    updateGL();
}

void WGLCanvas::compileShadersReaction(){
    updateGL();
}


void WGLCanvas::autoRotateModelReaction(){
    bool updateRequired = false;
    ConfigMgr &mgr =  Singleton<ConfigMgr>::instance();
    if(mgr.queryPropertyAs<bool>("display.modelRotation")){
        _cam->mCamPos.x = _cam->mCamPos.x > 360.f ? .0f : _cam->mCamPos.x+1.f;
        updateRequired = true;
    }

    if(mgr.queryPropertyAs<bool>("display.lightsRotation")){
        _lightsRotAngle = _lightsRotAngle > 360.f ? 0.f : _lightsRotAngle += 1.f;
        const float angleRad = _lightsRotAngle * PI / 180.f;

        const float lpos0x = mgr.queryPropertyAs<float>("render.light0positionX");
        const float lpos0z = mgr.queryPropertyAs<float>("render.light0positionZ");
        const float lpos1x = mgr.queryPropertyAs<float>("render.light1positionX");
        const float lpos1z = mgr.queryPropertyAs<float>("render.light1positionZ");

        _sceneState->mLight0Position[0] = lpos0x*std::cos(angleRad) - lpos0z*std::sin(angleRad);
        _sceneState->mLight0Position[2] = lpos0x*std::sin(angleRad) + lpos0z*std::cos(angleRad);
        _sceneState->mLight1Position[0] = lpos1x*std::cos(angleRad) - lpos1z*std::sin(angleRad);
        _sceneState->mLight1Position[2] = lpos1x*std::sin(angleRad) + lpos1z*std::cos(angleRad);
        updateRequired = true;
    }

    if(updateRequired)updateGL();
}

void WGLCanvas::loadMeshReaction(const QString &filename){
    _mesh->reset(this);
    _mesh->loadFromFile(filename.toStdString(), this);
}

