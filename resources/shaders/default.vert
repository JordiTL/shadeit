#version 120

varying vec3 normal;                   	// Vertex Normal
varying vec3 normal_world_position;		// Vertex Normal in world coord
varying vec3 vertex_world_position;		// Vertex Position in world coord
varying vec3 vertex_to_light0_vector;  	// Vertex to Light 0 vector
varying vec3 observer_world_direction; 	// Observer Position in world coord

void main(){
    // Vertex Position on the screen
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    	
    // Vertex Position in the world
    vertex_world_position = vec3(gl_ModelViewMatrix * gl_Vertex);
    	
    // Vertex Normal in world coordinates
    normal_world_position = normalize(gl_Normal*gl_NormalMatrix);
    	
    // Vertex to Light 0 vector
    vertex_to_light0_vector = normalize(gl_LightSource[0].position.xyz - vertex_world_position);
    	
    // Raw Vertex Normal
    normal = normalize(gl_Normal);
    	
    // Vertex color
    gl_FrontColor = gl_Color;
    	
    // Observer Direction in World Coordinates
    observer_world_direction.x = gl_ModelViewMatrix[0][2];
    observer_world_direction.y = gl_ModelViewMatrix[1][2];
    observer_world_direction.z = gl_ModelViewMatrix[2][2];
	
    // Texture Coordinates
    gl_TexCoord[0] = gl_MultiTexCoord0;
}