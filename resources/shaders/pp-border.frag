#version 120

uniform sampler2D colorMap;
uniform sampler2D depthMap;


void main()
{
	//Define fragment color
	gl_FragColor = texture2D(colorMap, gl_TexCoord[0].xy);
}