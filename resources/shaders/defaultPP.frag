#version 120
 
uniform sampler2D colorMap;		//<! Offscreen rendered scene color map
uniform sampler2D depthMap;		//<! Offscreen rendered scene depth buffer texture

void main(){
	gl_FragColor = texture2D(colorMap, gl_TexCoord[0].xy);
}