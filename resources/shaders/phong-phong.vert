#version 120

varying vec3 normal;
varying vec3 eyePos;
varying vec2 texCoord;

void main()
{
	// Vertex Position on the screen
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	// Vertex Position in the world
	eyePos = vec3(gl_ModelViewMatrix * gl_Vertex);
	
	// Vertex Normal
	normal = gl_NormalMatrix * gl_Normal;

	texCoord = gl_MultiTexCoord0.xy;
}