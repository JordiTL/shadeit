#version 120

void main()
{
	// Vertex Position
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	// Texture Coordinates
	gl_TexCoord[0] = gl_MultiTexCoord0;
}