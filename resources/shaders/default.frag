#version 120
#define MAX_LIGHTS 1

varying vec3 normal;                   	// Vertex Normal
varying vec3 normal_world_position;		// Vertex Normal in world coord
varying vec3 vertex_world_position;		// Vertex Position in world coord
varying vec3 vertex_to_light0_vector;  	// Vertex to Light 0 vector
varying vec3 observer_world_direction; 	// Observer Position in world coord

uniform sampler2D colorMap;
 
void main(){
	gl_FragColor = gl_Color;
}