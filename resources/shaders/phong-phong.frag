#version 120
#define MAX_LIGHTS 1

varying vec3 normal;
varying vec3 eyePos;
varying vec2 texCoord;

uniform sampler2D colorMap;

void main()
{
	gl_FragColor = vec4(0.0,0.0,0.0,1.0);
  
	for(int i = 0; i < MAX_LIGHTS; i++){ 
		vec3 normalVec = normalize(normal);
		vec3 lightDir = normalize(gl_LightSource[i].position.xyz - eyePos);
		vec3 reflectDir = reflect(-lightDir, normalVec);
		vec3 viewDir = normalize(-eyePos.xyz);
		
		// Calculate Ambient Component
		vec3 ambient = gl_FrontMaterial.ambient.xyz *texture2D(colorMap, texCoord).xyz* gl_LightSource[i].ambient.xyz;
		
		// Calculate Diffuse Component
		float dcont = max(0.0,dot(normalVec,lightDir));	
		
		// Diffuse Contribution									
		vec3 diffuse = dcont * gl_FrontMaterial.diffuse.xyz *texture2D(colorMap, texCoord).xyz* gl_LightSource[i].diffuse.xyz;

		// Calculate Specular Component
		float scont = pow(max(0.0,dot(viewDir,reflectDir)), gl_FrontMaterial.shininess);	//<! Specular Contribution
		vec3 specular = scont * gl_LightSource[i].specular.xyz * gl_FrontMaterial.specular.xyz;
		
		// Color addition
		gl_FragColor+=vec4((ambient+diffuse+specular), 0.0);
	}
}