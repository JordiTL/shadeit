/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef SHADERMGR_H
#define SHADERMGR_H

#include <QObject>
#include <QGLShader>
#include <QGLShaderProgram>

class ShaderMgr : public QObject
{
    Q_OBJECT

public:
    enum ShaderType{
        ST_VERTEX,
        ST_FRAGMENT,
        ST_PP_VERTEX,
        ST_PP_FRAGMENT
    };

    enum ShaderGroup{
        SG_GENERIC,
        SG_POSTPRODUCTION
    };

private:
    QGLShader *_vertShader;
    QGLShader *_fragShader;
    QGLShader *_ppvertShader;
    QGLShader *_ppfragShader;

    QString _vertShaderCode;
    QString _fragShaderCode;
    QString _ppvertShaderCode;
    QString _ppfragShaderCode;

    QGLShaderProgram _genericProgram;
    QGLShaderProgram _postprodProgram;

public:
    explicit ShaderMgr(QObject *parent = 0);
    ~ShaderMgr();

    bool compileSourceCode(ShaderType type, QString& code);
    bool compileSourceFile(ShaderType type, QString& file);

    QString getShaderCode(ShaderType type);
    unsigned int getPPSamplerLocation(int index);
    bool link(ShaderGroup group);
    void getCompilationLog(ShaderType type, QString& log);
    void getCompilationLog(ShaderGroup type, QString& log);

    void bindUniformVector(const QString& tag, float * values);

    void useProgram(ShaderGroup group);
    void releaseProgram(ShaderGroup group);

signals:

public slots:

};

#endif // SHADERMGR_H
