/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef WGLCANVAS_H
#define WGLCANVAS_H

#include <QGLWidget>
#include <QWidget>
#include <QGLShaderProgram>
#include <QString>
#include <QOpenGLDebugLogger>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QColor>
#include <QGLFramebufferObject>
#include <QGLFunctions>
#include <QTimer>
#include "Scene.h"
#include "vector3.h"

class WGLCanvas : public QGLWidget
{
    Q_OBJECT
private:
    Scene * _mesh;

    struct Camera3D{
        struct{
            double x = 2.0;
            double y = 2.0;
            double z = 5.0;
        } mCamPos;

        struct{
            double x = 0.0;
            double y = 0.0;
            double z = 0.0;
        } mCamLookAt;
    } *_cam;

    struct MouseState{
        struct{
            double x = 0.0;
            double y = 0.0;
            double z = 0.0;
        } mLastPos;

        double mLastWheel = 0.0;

        bool mInitialized = false;
    } *_mouse;

    struct SceneState{
        float mLight0Position[3];
        float mLight0Ambient[3];
        float mLight0Diffuse[3];
        float mLight0Specular[3];

        float mLight1Position[3];
        float mLight1Ambient[3];
        float mLight1Diffuse[3];
        float mLight1Specular[3];

        float mMaterialAmbient[3];
        float mMaterialDiffuse[3];
        float mMaterialSpecular[3];

        float mMaterialShininess = 0.f;
    } *_sceneState;

    struct GLCONTEXT_INFO{
        GLuint fbo;			//!< Frame Buffer Object
        GLuint img;			//!< FBO Texture
        GLuint imgDepth;
        GLuint rbo;			//!< Render Buffer Object
    } _glInfo;

    QColor _scapeColor;

    QGLFunctions _glFunc;

    QTimer *_modelRotTimer;

    float _modelRotAngle;
    float _lightsRotAngle;

public:
    explicit WGLCanvas(QWidget *parent = 0);
    ~WGLCanvas();

    void resizeGL(int width, int height);
    void initializeGL();
    void paintGL();
    void update();

    void mouseMoveEvent(QMouseEvent *_event);
    void wheelEvent(QWheelEvent * event);

signals:
    void updateShaderLogAction(const QString& log);
    void updateVertShaderViewerAction(const QString& code);
    void updateFragShaderViewerAction(const QString& code);
    void updatePPVertShaderViewerAction(const QString& code);
    void updatePPFragShaderViewerAction(const QString& code);


public slots:
    void useShaderReaction(QGLShaderProgram& program);
    void glMessageLoggedReaction(const QOpenGLDebugMessage &msg);
    void compileShadersReaction();

    void autoRotateModelReaction();

    void loadMeshReaction(const QString& filename);
};

#endif // WGLCANVAS_H
