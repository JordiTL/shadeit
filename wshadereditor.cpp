/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "wshadereditor.h"
#include "ui_wshadereditor.h"
#include <QString>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include "glslhighlighter.h"
#include "loghighlighter.h"
#include "singleton.h"
#include "shadermgr.h"

QString WShaderEditor::DEFAULT_VERTEX_SHADER = "DEFAULT VERTEX SHADER";
QString WShaderEditor::DEFAULT_FRAGMENT_SHADER = "DEFAULT FRAGMENT SHADER";
QString WShaderEditor::DEFAULT_PP_VERTEX_SHADER = "DEFAULT PP VERTEX SHADER";
QString WShaderEditor::DEFAULT_PP_FRAGMENT_SHADER = "DEFAULT PP FRAGMENT SHADER";


WShaderEditor::WShaderEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WShaderEditor)
{
    ui->setupUi(this);

    _vertHL = new GLSLHighlighter(ui->vertexShaderEditor->document());
    _fragHL = new GLSLHighlighter(ui->fragShaderEditor->document());
    _ppvertHL = new GLSLHighlighter(ui->vertexPPEditor->document());
    _ppfragHL = new GLSLHighlighter(ui->fragPPEditor->document());
    _logHL = new LogHighlighter(ui->logger->document());

    ui->vertexShaderEditor->setAcceptRichText(false);
    ui->fragShaderEditor->setAcceptRichText(false);
    ui->vertexPPEditor->setAcceptRichText(false);
    ui->fragPPEditor->setAcceptRichText(false);

    _onCreation();
}

WShaderEditor::~WShaderEditor()
{
    delete ui;

    delete _vertHL;
    delete _fragHL;
    delete _ppvertHL;
    delete _ppfragHL;
    delete _logHL;
}

void WShaderEditor::_onCreation(){
    loadDefaultFragmentShaderReaction();
    loadDefaultPPFragmentShaderReaction();
    loadDefaultPPVertexShaderReaction();
    loadDefaultVertexShaderReaction();
}

void WShaderEditor::loadDefaultVertexShaderReaction(){
    ui->vertexShaderEditor->setPlainText(DEFAULT_VERTEX_SHADER);
}

void WShaderEditor::loadDefaultFragmentShaderReaction(){
    ui->fragShaderEditor->setPlainText(DEFAULT_FRAGMENT_SHADER);
}

void WShaderEditor::loadDefaultPPVertexShaderReaction(){
    ui->vertexPPEditor->setPlainText(DEFAULT_PP_FRAGMENT_SHADER);
}

void WShaderEditor::loadDefaultPPFragmentShaderReaction(){
    ui->fragPPEditor->setPlainText(DEFAULT_PP_VERTEX_SHADER);
}

void WShaderEditor::updateShaderLogReaction(const QString &log){
    ui->logger->clear();
    QString copylog(log);
    ui->logger->setPlainText(copylog);
}

void WShaderEditor::appendShaderLogReaction(const QString &log){
    QString lastlog = ui->logger->toPlainText();
    QString copylog(log);
    lastlog.append(copylog);
    ui->logger->setPlainText(lastlog);
}

void WShaderEditor::updateVertShaderViewerReaction(const QString& code){
    ui->vertexShaderEditor->setPlainText(code);
}

void WShaderEditor::updateFragShaderViewerReaction(const QString& code){
    ui->fragShaderEditor->setPlainText(code);
}

void WShaderEditor::updatePPVertShaderViewerReaction(const QString& code){
    ui->vertexPPEditor->setPlainText(code);
}

void WShaderEditor::updatePPFragShaderViewerReaction(const QString& code){
    ui->fragPPEditor->setPlainText(code);
}

void WShaderEditor::compileShadersReaction(){
    ShaderMgr& smgr = Singleton<ShaderMgr>::instance();

    QString code;
    code = ui->vertexShaderEditor->toPlainText();
    if(smgr.compileSourceCode(ShaderMgr::ST_VERTEX, code))
        ui->tabsEditors->setProperty("errorOnVertEditor", false);
    else
        ui->tabsEditors->setProperty("errorOnVertEditor", true);

    code = ui->fragShaderEditor->toPlainText();
    if(smgr.compileSourceCode(ShaderMgr::ST_FRAGMENT, code))
        ui->tabsEditors->setProperty("errorOnFragEditor", false);
    else
        ui->tabsEditors->setProperty("errorOnFragEditor", true);
    code = ui->vertexPPEditor->toPlainText();

    if(smgr.compileSourceCode(ShaderMgr::ST_PP_VERTEX, code))
        ui->tabsEditors->setProperty("errorOnPPVertEditor", false);
    else
        ui->tabsEditors->setProperty("errorOnPPVertEditor", true);
    code = ui->fragPPEditor->toPlainText();

    if(smgr.compileSourceCode(ShaderMgr::ST_PP_FRAGMENT, code))
        ui->tabsEditors->setProperty("errorOnPPFragEditor", false);
    else
        ui->tabsEditors->setProperty("errorOnPPFragEditor", true);


    QString msg = "Compilation Results:\n";
    QString log;
    updateShaderLogReaction(msg);

    msg = "<Vertex Shader>\n";
    smgr.getCompilationLog(ShaderMgr::ST_VERTEX,log);
    msg.append(log);
    msg.append("\n\n");
    appendShaderLogReaction(msg);

    log.clear();
    msg = "<Fragment Shader>\n";
    smgr.getCompilationLog(ShaderMgr::ST_FRAGMENT,log);
    msg.append(log);
    msg.append("\n\n");
    appendShaderLogReaction(msg);

    log.clear();
    msg = "<Post Production Vertex Shader>\n";
    smgr.getCompilationLog(ShaderMgr::ST_PP_VERTEX,log);
    msg.append(log);
    msg.append("\n\n");
    appendShaderLogReaction(msg);

    log.clear();
    msg = "<Post Production Fragment Shader>\n";
    smgr.getCompilationLog(ShaderMgr::ST_PP_FRAGMENT,log);
    msg.append(log);
    msg.append("\n\n");
    appendShaderLogReaction(msg);

    Singleton<ShaderMgr>::instance().link(ShaderMgr::SG_GENERIC);
    Singleton<ShaderMgr>::instance().link(ShaderMgr::SG_POSTPRODUCTION);



    emit compileShadersAction();
}

void WShaderEditor::saveVertShaderReaction(const QString& filename){
    QString code(ui->vertexShaderEditor->toPlainText());

    _saveTextFileShader(filename, code);
}

void WShaderEditor::saveFragShaderReaction(const QString& filename){
    QString code(ui->fragShaderEditor->toPlainText());
    _saveTextFileShader(filename, code);
}

void WShaderEditor::savePPVertShaderReaction(const QString& filename){
    QString code(ui->vertexPPEditor->toPlainText());
    _saveTextFileShader(filename, code);
}

void WShaderEditor::savePPFragShaderReaction(const QString& filename){
    QString code(ui->fragPPEditor->toPlainText());
    _saveTextFileShader(filename, code);
}

bool WShaderEditor::_saveTextFileShader(const QString& path, const QString& code){
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        QMessageBox::information(0,"Error saving file",file.errorString());
        qDebug()<< "Error saving file: " << path;
        return false;
    }

    QTextStream out(&file);
    out << code;

    return true;
}

void WShaderEditor::changeDescriptionReaction(int currentTab){
    switch(currentTab){
    case 0:
        ui->ppfragmentDesc->hide();
        ui->ppfragmentTitle->hide();
        ui->ppvertexDesc->hide();
        ui->ppvertexTitle->hide();
        ui->fragmentDesc->hide();
        ui->fragmentTitle->hide();
        ui->vertexDesc->show();
        ui->vertexTitle->show();
        break;
    case 1:
        ui->ppfragmentDesc->hide();
        ui->ppfragmentTitle->hide();
        ui->ppvertexDesc->hide();
        ui->ppvertexTitle->hide();
        ui->fragmentDesc->show();
        ui->fragmentTitle->show();
        ui->vertexDesc->hide();
        ui->vertexTitle->hide();
        break;
    case 2:
        ui->ppfragmentDesc->hide();
        ui->ppfragmentTitle->hide();
        ui->ppvertexDesc->show();
        ui->ppvertexTitle->show();
        ui->fragmentDesc->hide();
        ui->fragmentTitle->hide();
        ui->vertexDesc->hide();
        ui->vertexTitle->hide();
        break;
    case 3:
        ui->ppfragmentDesc->show();
        ui->ppfragmentTitle->show();
        ui->ppvertexDesc->hide();
        ui->ppvertexTitle->hide();
        ui->fragmentDesc->hide();
        ui->fragmentTitle->hide();
        ui->vertexDesc->hide();
        ui->vertexTitle->hide();
        break;
    }
}


