/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    bool _loadTextFileShader(const QString& path, QString& code);


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

signals:
    void updateShaderLogAction(const QString& log);
    void updateVertShaderViewerAction(const QString& code);
    void updateFragShaderViewerAction(const QString& code);
    void updatePPVertShaderViewerAction(const QString& code);
    void updatePPFragShaderViewerAction(const QString& code);

    void saveVertShaderAction(const QString& filename);
    void saveFragShaderAction(const QString& filename);
    void savePPVertShaderAction(const QString& filename);
    void savePPFragShaderAction(const QString& filename);


    void updateGLAction();

    void loadMeshAction(const QString& filename);

public slots:
    void updateShaderLogReaction(const QString& log);
    void updateVertShaderViewerReaction(const QString& code);
    void updateFragShaderViewerReaction(const QString& code);
    void updatePPVertShaderViewerReaction(const QString& code);
    void updatePPFragShaderViewerReaction(const QString& code);

    void openVertexShaderReaction();
    void openFragmentShaderReaction();
    void openPPVertexShaderReaction();
    void openPPFragmentShaderReaction();

    void saveVertShaderReaction();
    void saveFragShaderReaction();
    void savePPVertShaderReaction();
    void savePPFragShaderReaction();

    void openCustomSceneReaction();

    void exitReaction();
    void loadDefaultsReaction();

    void showAboutUsReaction();

    void loadCubeReaction();
    void loadKnotReaction();
    void loadUvSphereReaction();
    void loadIcoSphereReaction();
    void loadPlaneReaction();
    void loadSuzanneReaction();
    void loadBunnyReaction();
    void loadLinkReaction();
    void loadTorusReaction();
    void loadTeapotReaction();
};

#endif // MAINWINDOW_H
