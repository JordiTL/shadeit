/** Copyright (c) Jorge Torregrosa <jtorregrosalloret@gmail.com>
  * This file is part of ShadeIt!.

  * ShadeIt! is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * ShadeIt! is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with ShadeIt!.  If not, see <http://www.gnu.org/licenses/>.
  */

#include "gldebug.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <QDebug>
#include <string>

#ifdef GLDEBUG
void printErrorOpenGL(char *file, int line){
    unsigned int err = GL_NO_ERROR;
    do{
          err = glGetError();
          if(err != GL_NO_ERROR){
            qDebug() << "glError in file " << file << " @ line "<< line << ": " << QString(reinterpret_cast<const char *>(gluErrorString(err)));
          }
    }while(err != GL_NO_ERROR);
}
#endif
